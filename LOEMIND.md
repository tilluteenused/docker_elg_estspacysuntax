# Konteiner eesti keele morfoloogilise ja süntaktilise analüüsi tegemiseks spaCy töövoona

SpaCy töövoogu sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).


## Mida sisaldab <a name="Mida_sisaldab"></a>

* [UD v2.5 eesti puudepangal](https://github.com/UniversalDependencies/UD_Estonian-EDT) treenitud ja keelemudelit [xml-roberta-base](https://huggingface.co/xlm-roberta-base) sisaldav [SpaCy töövoog eesti keele morfoloogiliseks ja süntaktiliseks märgendamiseks](https://github.com/EstSyntax/EstSpaCy); seejuures tehakse ka sõnestamine ja lausestamine; morfoloogilised kategooriad on [UD](https://universaldependencies.org/)-kohased. 
* Konteineri ja liidesega seotud lähtekood


## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).

## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/elg_estspacysuntax:2022.08.08
```
Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. [Lähtekoodi](https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git) allalaadimine

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git gitlab_docker_elg_estspacysuntax
```

### 2. Konteineri kokkupanemine 

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_estspacysuntax
docker build -t tilluteenused/elg_estspacysuntax:2022.08.08 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

```commandline
docker run -p 8000:8000 tilluteenused/elg_estspacysuntax:2022.08.08
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

```json
{
  "type":"text",
  "content": string, /* Analüüsitav tekst */
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [ /* laused tekstis */
      {
        "texts":
        [ /* sõned lauses */
          {
            "content": string /* sõne */
            "features":
            {
              "i": number,      /* sõne id */
              "pos": string,    /* sõnaliik */
              "morph": string,  /* morf info */
              "head": string,   /* Link to the parent in the dependency tree, or null for root node */
              "dep": string,    /* Kaare märgend {"ROOT"|"obj"|"punct"|"prt"|...} */
            }
          }
        ]
      }
    ]
  }
}
```
## Kasutusnäide

```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' http://localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.2.1 Python/3.7.12
Date: Mon, 08 Aug 2022 09:49:20 GMT
Content-Type: application/json
Content-Length: 502
Connection: close

{"response":{"type":"texts","texts":[{"texts":[{"content":"Koer","features":{"i":0,"pos":"NOUN","morph":"Case=Nom|Number=Sing","head":"1","dep":"nsubj"}},{"content":"leidis","features":{"i":1,"pos":"VERB","morph":"Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin|Voice=Act","head":null,"dep":"ROOT"}},{"content":"kondi","features":{"i":2,"pos":"NOUN","morph":"Case=Gen|Number=Sing","head":"1","dep":"obj"}},{"content":".","features":{"i":3,"pos":"PUNCT","morph":"","head":"1","dep":"punct"}}]}]}}
```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep 

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
