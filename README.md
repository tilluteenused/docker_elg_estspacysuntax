# Container of a spaCy pipeline for Estonian morphological and syntactic analysis 

A spaCy pipeline container (docker) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).


## Contains  <a name="Contains"></a>

* [SpaCy pipeline for morphological and syntactic analysis of Estonian](https://github.com/EstSyntax/EstSpaCy), trained on 
[UD v2.5 Estonian treebank](https://github.com/UniversalDependencies/UD_Estonian-EDT) and using [xml-roberta-base](https://huggingface.co/xlm-roberta-base) language model; includes tokenization and sentence splitting; outputs [UD](https://universaldependencies.org/)-features as [morphological categories](https://cl.ut.ee/ressursid/morfo-systeemid/index.php?lang=en). 
* Container and interface code


## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/elg_estspacysuntax:2022.08.08
```
Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading [source code](https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git) 

```commandline
mkdir -p ~/gitlab-docker-elg

cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_estspacysuntax.git gitlab_docker_elg_estspacysuntax
```

### 2. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab_docker_elg_estspacysuntax
docker build -t tilluteenused/elg_estspacysuntax:2022.08.08 .
```

## Starting the container <a name="Starting_the_container"></a>

```commandline
docker run -p 8000:8000 tilluteenused/elg_estspacysuntax:2022.08.08
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json

```json
{
  "type":"text",
  "content": string, /* The text of the request */
}
```

## Response json

```json
{
  "response":
  {
    "type":"texts",
    "texts":
    [ /* array of sentences */
      {
        "texts"
        [ /* array of tokens */
          {
            "content": string /* token */
            "features":
            {
              "i": number,     /* Identifier for this token */
              "pos": string,   /* Universal Dependencies coarse level POS tag */
              "morph": string, /* Morphological info */
              "head": string,  /* Link to the parent in the dependency tree, or null for root node */
              "dep": string,   /* {"ROOT"|"obj"|"punct"|"prt"|...} */
            }
          }
        ]
      }
    ]
  }
}
```
## Usage example


```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' http://localhost:8000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.2.1 Python/3.7.12
Date: Mon, 08 Aug 2022 09:49:20 GMT
Content-Type: application/json
Content-Length: 502
Connection: close

{"response":{"type":"texts","texts":[{"texts":[{"content":"Koer","features":{"i":0,"pos":"NOUN","morph":"Case=Nom|Number=Sing","head":"1","dep":"nsubj"}},{"content":"leidis","features":{"i":1,"pos":"VERB","morph":"Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin|Voice=Act","head":null,"dep":"ROOT"}},{"content":"kondi","features":{"i":2,"pos":"NOUN","morph":"Case=Gen|Number=Sing","head":"1","dep":"obj"}},{"content":".","features":{"i":3,"pos":"PUNCT","morph":"","head":"1","dep":"punct"}}]}]}}
```

## Sponsors

The container development was sponsored by EU CEF project [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)


## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
